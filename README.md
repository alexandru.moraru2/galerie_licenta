# galerie_licenta

Pasi pentru instalare:

1. Instalare XAMPP
https://www.apachefriends.org/

2. Clonare repozitoriu in ../xampp/htdocs/

3. Creare folder pentru imagini temp și temp

4. Creare baza de date cu numele "aikive" și următoarele tabele
 - utilizatori, cu câmpurile user varchar(10), privilege int(1) și hash varchar(255)
 - likes, cu câmpurile id int(10), user varchar(10)
 - comments, cu câmpurile comment_id int(10) cu auto increment, user varchar(10), id int(10), data datetime și comment varchar(500)
 - imagini, cu câmpurile id int(10), url varchar(40), tags varchar(200), uploader varchar(100), sha256 varchar(64) și date date

 5. Instalare stable diffusion web ui conform instrucțiunilor de mai jos
 https://github.com/AUTOMATIC1111/stable-diffusion-webui

 6. Adaugare în webui-user.bat la COMMANDLINEARGS: "--api", pentru activarea api-ului

 7. Pornire server Apache și MySQL în XAMPP

 8. Pornire server stable diffusion prin executarea webui-user.bat
